from django.http import JsonResponse
from .models import Blog, Comment
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from .acls import get_fun_fact

class CommentListEncoder(ModelEncoder):
    model = Comment
    properties = ["content"]

class BlogListEncoder(ModelEncoder):
    model = Blog
    properties = ["title"]

class BlogDetailEncoder(ModelEncoder):
    model = Blog
    properties = ["title", "content", "answer", "question", "category"]


@require_http_methods(["GET", "POST"])
def list_blogs(request):
    if request.method == "GET":
        # Get all of the blogs
        blogs = Blog.objects.all()
        # Return the blogs in a JsonResponse
        return JsonResponse({"blogs": blogs}, encoder=BlogListEncoder)
    else:
        content = json.loads(request.body)


        fact = get_fun_fact()
        content.update(fact)
        
        # use dictionary to create new blos
        blog = Blog.objects.create(**content)
        return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)

@require_http_methods(["GET", "PUT", "DELETE"])
def show_blog_detail(request, pk):
    if request.method == "GET":
    # Get the blog
        blog = Blog.objects.get(pk=pk)
        # Return the JsonResponse
        return JsonResponse({"blog": blog}, encoder=BlogDetailEncoder)
    elif request.method == "DELETE":
        count, _ = Blog.objects.filter(id=pk).delete()
        print("count: ", count)
        print("_", _)
        return JsonResponse({"delete": count > 0})
    
    else:
        content = json.loads(request.body)
        Blog.objects.filter(id=pk).update(**content)
        blog = Blog.objects.get(id=pk)
        return JsonResponse({"blog": blog}, encoder=BlogListEncoder)
        

def create_comment(request, blog_id):
    if request.method == "POST":
        blog = Blog.objects.get(id=blog_id)
        content = json.loads(request.body)
        Comment.objects.create(
            content=content["comment"],
            blog=blog,
        )
        return JsonResponse({"message": "created"})
