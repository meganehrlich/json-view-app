import json
import requests

def get_fun_fact():
    response = requests.get("https://jservice.xyz/api/random-clue?valid=true")
    clue = json.loads(response.content)

    fun_fact = {
        "answer": clue["answer"],
        "question": clue["question"],
        "category": clue["category"]["title"],
    }

    return fun_fact